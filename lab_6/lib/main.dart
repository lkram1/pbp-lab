import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
      theme: ThemeData(
        brightness: Brightness.light,
        fontFamily: 'Helvetica',
      ),
      home: MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Tasks tracker"),
          backgroundColor: Colors.pink,
        ),
        body: SingleChildScrollView(
            child: Container(
          color: Colors.pink[300],
          padding: EdgeInsets.fromLTRB(20, 40, 20, 0),
          child: Column(
            children: [
              Container(
                height: 200,
                padding: EdgeInsets.all(30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Heloo ikram,\nWelcome back!",
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                        )),
                    Container(
                      width: 100,
                      height: 100,
                      child: Center(
                        child: Text(
                          "5",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 24,
                          ),
                        ),
                      ),
                      decoration: BoxDecoration(
                        color: Colors.purple[400],
                        shape: BoxShape.circle,
                      ),
                    )
                  ],
                ),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(
                    const Radius.circular(8),
                  ),
                ),
              ),
              Divider(
                height: 10,
              ),
              Container(
                  height: 200,
                  padding: EdgeInsets.all(30),
                  child: Table(
                    defaultColumnWidth: FixedColumnWidth(120.0),
                    children: [
                      TableRow(children: [
                        Padding(
                          padding: EdgeInsets.all(10),
                          child: Text("Tasks",
                              style: TextStyle(fontWeight: FontWeight.bold)),
                        ),
                        Padding(
                          padding: EdgeInsets.all(10),
                          child: Text("Deadlines",
                              style: TextStyle(fontWeight: FontWeight.bold)),
                        ),
                        Padding(
                          padding: EdgeInsets.all(10),
                          child: Text("Status",
                              style: TextStyle(fontWeight: FontWeight.bold)),
                        ),
                      ]),
                      TableRow(children: [
                        Padding(
                          padding: EdgeInsets.all(10),
                          child: Text("Testing"),
                        ),
                        Padding(
                          padding: EdgeInsets.all(10),
                          child: Text("20-10-2020"),
                        ),
                        Padding(
                            padding: EdgeInsets.all(10),
                            child: Text(
                              "Completed",
                              style: TextStyle(
                                backgroundColor: Colors.green[600],
                                color: Colors.white,
                              ),
                            )),
                      ]),
                      TableRow(children: [
                        Padding(
                          padding: EdgeInsets.all(10),
                          child: Text("Testing 2"),
                        ),
                        Padding(
                          padding: EdgeInsets.all(10),
                          child: Text("20-10-2020"),
                        ),
                        Padding(
                            padding: EdgeInsets.all(10),
                            child: Text(
                              "On Going",
                              style: TextStyle(
                                backgroundColor: Colors.orange[600],
                                color: Colors.white,
                              ),
                            )),
                      ]),
                      TableRow(children: [
                        Padding(
                          padding: EdgeInsets.all(10),
                          child: Text("Testing 3"),
                        ),
                        Padding(
                          padding: EdgeInsets.all(10),
                          child: Text("20-10-2020"),
                        ),
                        Padding(
                            padding: EdgeInsets.all(10),
                            child: Text(
                              "Unfinished",
                              style: TextStyle(
                                backgroundColor: Colors.red[600],
                                color: Colors.white,
                              ),
                            )),
                      ]),
                    ],
                  ),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(
                      const Radius.circular(8),
                    ),
                  )),
              Divider(
                height: 10,
              ),
              Container(
                  height: 200,
                  padding: EdgeInsets.all(30),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(const Radius.circular(8))),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Expanded(
                            flex: 1,
                            child: Container(
                              padding: EdgeInsets.fromLTRB(3, 17, 3, 19),
                              decoration: BoxDecoration(
                                  color: Colors.green,
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(8),
                                      bottomLeft: Radius.circular(8))),
                            ),
                          ),
                          Expanded(
                            flex: 9,
                            child: Container(
                              child: Text("makan nasi",
                                  style: TextStyle(color: Colors.white)),
                              padding: EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                  color: Colors.purple[400],
                                  borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(8),
                                      bottomRight: Radius.circular(8))),
                            ),
                          )
                        ],
                      ),
                      Divider(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Expanded(
                            flex: 1,
                            child: Container(
                              padding: EdgeInsets.fromLTRB(3, 17, 3, 19),
                              decoration: BoxDecoration(
                                  color: Colors.red,
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(8),
                                      bottomLeft: Radius.circular(8))),
                            ),
                          ),
                          Expanded(
                            flex: 9,
                            child: Container(
                              child: Text("Tidur",
                                  style: TextStyle(color: Colors.white)),
                              padding: EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                  color: Colors.purple[400],
                                  borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(8),
                                      bottomRight: Radius.circular(8))),
                            ),
                          )
                        ],
                      ),
                      Divider(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Expanded(
                            flex: 1,
                            child: Container(
                              padding: EdgeInsets.fromLTRB(3, 17, 3, 19),
                              decoration: BoxDecoration(
                                  color: Colors.orange,
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(8),
                                      bottomLeft: Radius.circular(8))),
                            ),
                          ),
                          Expanded(
                            flex: 9,
                            child: Container(
                              child: Text("Mandi",
                                  style: TextStyle(color: Colors.white)),
                              padding: EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                  color: Colors.purple[400],
                                  borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(8),
                                      bottomRight: Radius.circular(8))),
                            ),
                          )
                        ],
                      )
                    ],
                  ))
            ],
          ),
        )));
  }
}
