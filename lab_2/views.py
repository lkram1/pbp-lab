from django.http.response import HttpResponse
from django.shortcuts import render
from django.http import HttpResponse
from .models import Note
from django.core import serializers

# Create your views here.

def html_view(request):
    note = Note.objects.all()
    response = {
        'notes':note
    }
    return render(request,'Notes.html',response)

def xml_view(request):
    note = Note.objects.all()
    note =serializers.serialize('xml',note)
    return HttpResponse(note,content_type='application/xml')

def json_view(request):
    note = Note.objects.all()
    note =serializers.serialize('json',note)
    return HttpResponse(note,content_type='application/json')
