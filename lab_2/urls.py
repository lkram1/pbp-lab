from django.urls import path
from .views import xml_view,html_view,json_view

urlpatterns = [
    path('html/',html_view,name='html'),
    path('xml/',xml_view,name="xml"),
    path("json/",json_view,name="json")
]