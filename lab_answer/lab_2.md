1. Apakah perbedaan antara JSON dan XML?
JSON dan XML memiliki fungsi yang sama yaitu untuk mentransfer data dalam format yang dapat dibaca. Perbedaanya adalah JSON (Javascript Object Notation ) digunakan untuk merepresentasikan suatu objek yang memiliki tipe (bisa berupa integer, string, boolean dll) sedangkan xml datanya bersifat string.

2. Apakah perbedaan antara HTML dan XML?
perbedaan HTML dan XLM adalah, HTML (Hypertext Markup Language) lebih fokus terhadap hubungan antar link di web dan untuk menampilkan data sehingga HTML bersifat static. Sedangkan XML (eXtensible Markup Language) fokus terhadap transfer data (bukan menampilkannya) sehingga bersifat dinamis.