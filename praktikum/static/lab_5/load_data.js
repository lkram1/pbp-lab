function clicked(id) {
    // get data sesuai dengan id ,terus replace
    let url_target = 'get_note/' + id
    $.ajax({
        url: url_target ,
        success : function(response) {

            $(".modal-title").html(
                `
                <h5> ${response[0].fields.title} </h5>
                `
            )

            $(".modal-body").html(
                `
                <h5 class='ms-2'> ${response[0].fields.froms} </h5>
                <h6 class='ms-2 text-info'> To ${response[0].fields.to} </h6>
                <p class='ms-2'> ${response[0].fields.message} </p>
                `
            )
        }
    })
}

function editData(id) {
    $.ajax({
        url :`update_note/${id}`,
        success : function(response)  {
            console.log(response)
        }
    })
}

$(document).ready(function () {
    $.ajax({
      url: "/lab-2/json/",
      success: function(response) {
        for (let i = 0;i<response.length;i++){

          $("tbody").append(
            `<tr>
              <td>${response[i].fields.froms}</td>
              <td>${response[i].fields.to}</td>
              <td>${response[i].fields.title}</td>
              <td>${response[i].fields.message}</td>
              <td>
              <button type="button" class="btn btn-primary m-1" data-toggle="modal" data-target="#viewModal" onclick=clicked(${response[i].pk})>
                  View
                </button>
                <button type="button" class="btn btn-warning m-1" data-toggle="modal" data-target="#editModal onclick=editData(${response[i].pk})">
                  Edit
                </button><button class='btn btn-danger m-1'>
                  Delete
                </button>
              </td>
            </tr>
            `
          )
      }
      },
    })
  })


