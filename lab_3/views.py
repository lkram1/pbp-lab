from django.contrib.auth import login
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from .forms import FriendForm
from lab_1.models import Friend
from django.contrib.auth.decorators import login_required
# Create your views here.

@login_required(login_url='/admin/login/')
def index(request):
    response = {'friends' : Friend.objects.all()}
    return render(request,'lab3_index.html',response)


@login_required(login_url='/admin/login/')
def add_friend(request):
    response = {}

    if request.method == "POST":
        form = FriendForm(request.POST)
        
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/lab-3')

    else:
        form = FriendForm()
        
    response['form'] = form
    return render(request,'lab3_form.html',response)