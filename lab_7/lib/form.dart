import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class MyForm extends StatefulWidget {
  const MyForm({Key? key}) : super(key: key);

  @override
  _MyFormState createState() => _MyFormState();
}

class _MyFormState extends State<MyForm> {
  // define state over here
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _pass = TextEditingController();
  final TextEditingController _confirmPass = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Form(
            key: _formKey,
            child: Container(
              padding: EdgeInsets.all(10),
              child: Column(
                children: [
                  TextFormField(
                    autofocus: true,
                    validator: (value) {
                      if (value != null && value.length == 0) {
                        return "masukkan username yang valid";
                      }
                      return null;
                    },
                    decoration: new InputDecoration(
                      hintText: "Masukkan username anda",
                      labelText: "Username",
                    ),
                  ),
                  TextFormField(
                    autofocus: true,
                    validator: (value) {
                      if (value != null && value.length == 0) {
                        return "masukkan nama yang valid";
                      }
                      return null;
                    },
                    decoration: new InputDecoration(
                      hintText: "Masukkan nama anda",
                      labelText: "Nama",
                    ),
                  ),
                  TextFormField(
                    obscureText: true,
                    enableSuggestions: false,
                    autocorrect: false,
                    autofocus: true,
                    controller: _pass,
                    validator: (value) {
                      if (value != null && value.length < 8) {
                        return "password harus memiliki panjang kurang lebih dari 8";
                      }
                    },
                    decoration: new InputDecoration(
                      hintText: "Masukkan passowrd anda",
                      labelText: "Password",
                    ),
                  ),
                  TextFormField(
                    controller: _confirmPass,
                    validator: (value) {
                      if (value != _pass.text) {
                        return "mohon masukkan password dengan tepat";
                      }
                    },
                    obscureText: true,
                    enableSuggestions: false,
                    autocorrect: false,
                    autofocus: true,
                    decoration: new InputDecoration(
                      hintText: "Konfirmasi password anda",
                      labelText: "password",
                    ),
                  ),
                  ElevatedButton(
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(content: Text('Processing Data')),
                          );
                        }
                      },
                      child: Text("Submit"))
                ],
              ),
            )));
  }
}

validateNama(value) {
  if (value == '') {
    return "masukkan nama yang valid!";
  }
  return null;
}
