function clicked(id) {
    // get data sesuai dengan id ,terus replace
    let url_target = 'get_note/' + id
    $.ajax({
        url: url_target ,
        success : function(response) {
            $(".modal-title").html(
                `
                <h5> ${response[0].fields.title} </h5>
                `
            )
            $(".modal-body").html(
                `
                <h5 class='ms-2'> ${response[0].fields.froms} </h5>
                <h6 class='ms-2 text-info'> To ${response[0].fields.to} </h6>
                <p class='ms-2'> ${response[0].fields.message} </p>
                `
            )
        }
    })
}


function editData(id) {
  $.ajax({
      url : `update_note/${id}`,
      success : function(response) {
          $("#id_to").val(`${response[0].fields.to}`)
          $("#id_froms").val(`${response[0].fields.froms}`)
          $("#id_message").val(`${response[0].fields.message}`)
          $("#id_title").val(`${response[0].fields.title}`)
      }
  })

  postData(id);
}

function postData(id){
  console.log(id);
  $('input[name="testing"]').on("click",function (e) {
    e.preventDefault();
    e.stopPropagation();
    const csrf = document.getElementsByName('csrfmiddlewaretoken')[0].value
    const serializedData = $(this).serialize();
    serializedData['csrfmiddlewaretoken'] = csrf;
    data = {}
    data['to'] = $('#id_to').val()
    data['froms'] = $('#id_froms').val()
    data['title'] = $('#id_title').val()
    data['message'] = $('#id_message').val()
    data['csrfmiddlewaretoken'] = csrf
    $.ajax({
        type: "POST",
        url : `update_note/${id}`,
        data: data,
        success: function(response) {
            console.log(id)
            let instance = JSON.parse(response['instance']);
            let fields = instance[0]['fields'];
            $(`#to-${id}`).text(`${fields['to']}`)
            $(`#froms-${id}`).text(`${fields['froms']}`)
            $(`#message-${id}`).text(`${fields['message']}`)
            $(`#title-${id}`).text(`${fields['title']}`)
        },
        error: function (response) {
        }
    })
})
} 

$(document).ready(function () {
    $.ajax({
      url: "/lab-2/json/",
      success: function(response) {
        for (let i = 0;i<response.length;i++){
          $("tbody").append(
            `<tr>
              <td id='froms-${response[i].pk}'>${response[i].fields.froms}</td>
              <td id='to-${response[i].pk}'>${response[i].fields.to}</td>
              <td id='title-${response[i].pk}'>${response[i].fields.title}</td>
              <td id='message-${response[i].pk}'>${response[i].fields.message}</td>
              <td>
              <button 
                type="button" 
                class="btn btn-primary m-1" 
                data-toggle="modal" 
                data-target="#viewModal" 
                onclick=clicked(${response[i].pk})
              >
                  View
                </button>
                <button 
                    type="button" 
                    class="btn 
                    btn-warning m-1" 
                    data-toggle="modal" 
                    data-target="#editModal" 
                    onclick='editData(${response[i].pk})`'
                >
                  Edit
                </button><button class='btn btn-danger m-1'>
                  Delete
                </button>
              </td>
            </tr>
            `
          )
      }
      },
    })
  })


