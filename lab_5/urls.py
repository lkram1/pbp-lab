from django.urls import path
from .views import index,get_note, update_note

urlpatterns = [
    path('',index,name='testing'),
    path('get_note/<id>',get_note,name='get-note'),
    path('update_note/<id>',update_note,name='update-note'),
    
]