from django.http.response import HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse
from lab_2.models import Note
from django.core import serializers
from .forms import NoteForm

# Create your views here.

def index(request):
    response = {'notes' : Note.objects.all(),'form':NoteForm()}
    return render(request,'lab5_index.html',response)

def get_note(request,id):
    note = Note.objects.filter(id=id)
    note =serializers.serialize('json',note)
    return HttpResponse(note,content_type='application/json')

def update_note(request,id):
    note = get_object_or_404(Note,id=id)

    if request.is_ajax and request.method == "POST":
        form = NoteForm(request.POST,instance=note)
        if form.is_valid():
            instance = form.save()
            ser_instance = serializers.serialize('json',[instance,])
            return JsonResponse({'instance' : ser_instance},status=200)

        else :
            return JsonResponse({"error" : form.errors} ,status=400)

    else:
        data = {'froms':note.froms,'to':note.to,'title':note.title,'message':note.message}
        form = NoteForm(initial=data)

    note = serializers.serialize('json',[note,])
    return HttpResponse(note,content_type='application/json')