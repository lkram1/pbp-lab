from django.http import response
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from lab_2.models import Note
from .forms import NoteForm

# Create your views here.

def index(request):
    response = {
        'notes' : Note.objects.all()
    }

    return render(request,'lab4_index.html',response)


def add_note(request):
    response = {}

    if request.method == "POST":
        form = NoteForm(request.POST)
        if form.is_valid() :
            form.save()

            return HttpResponseRedirect('/lab-4')
    
    else:
        form  = NoteForm()
    
    response['form'] = form
    
    return render(request,'lab4_form.html',response)

def note_list(request):
    response = {}
    response['notes'] = Note.objects.all()

    return render(request,'lab4_note_list.html',response)